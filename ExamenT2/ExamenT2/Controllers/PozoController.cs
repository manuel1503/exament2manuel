﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entidades;
using Interfaces;



namespace ExamenT2.Controllers
{
    public class PozoController : Controller
    {
        //
        // GET: /Pozo/

        private IPozo Repository;
        public PozoController(IPozo Repository)
        {
            this.Repository = Repository;


        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MostrarFormulario()
        {
            return View();
        
        }

        [HttpGet]
        public ViewResult Index(String query = "")
        {
            var datos = Repository.ByQueryAll(query);
            return View("CreatePozo",datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarPozo");
        }

        [HttpPost]
        public ActionResult Create(Pozo pozo)
        {

          
                Repository.Store(pozo);

                TempData["UpdateSuccess"] = "Se creó correctamente";

                
            
            return View("ListaDePozos", pozo);
        }

        [HttpPost]
        public ActionResult AsignarPozoPersona()
        {

            return View("AsignarPozoPersona");
        
        }

    }
}
