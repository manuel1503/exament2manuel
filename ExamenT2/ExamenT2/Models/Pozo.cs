﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenT2.Models
{
    public class Pozo
    {
        public string Nombre { get; set; }
        public string Propietario { get; set; }
        public string Ubicacion { get; set; }
        public string CoordenadaEste { get; set; }
        public string CoordenadaNorte { get; set; }
    }
}