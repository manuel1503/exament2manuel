﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Pozo
    {
        public string Nombre { get; set; }
        public string Propietario { get; set; }
        public string Ubicacion { get; set; }
        public string CoordenadaEste { get; set; }
        public string CoordenadaNorte { get; set; }
    }
}
