﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenT2.Controllers;
using NUnit.Framework;
using Moq;
using ExamenT2;
using Entidades;
using Interfaces;
using System.Web.Mvc;

namespace ExamenT2.Testing
{
    [TestFixture]
    public class ControladoresTest
    {
        [Test]
        public void RetornaVista()
        {
            var mock = new Mock<IPozo>();
            mock.Setup(o => o.ListarPozos()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());

            var controller = new PozoController(mock.Object);
            var view = controller.Index();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("CreatePozo", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Pozo>), view.Model);
        }

        [Test]
        public void TestCreateReturnView()
        {
            var controller = new PozoController(null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("RegistrarPozo", view.ViewName);
        }


        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<IPozo>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var controller = new PozoController(repositoryMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<IPozo>();

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new PozoController(repositoryMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());
        }

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var controller = new PozoController(null);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }
        [Test]
        public void TestAsignarPersonaAPozo()
        {
            var pozo = new Pozo { };

            var mock = new Mock<IPozo>();
            mock.Setup(o => o.ListarPozos()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarPersonaAPozo(pozo, "Lucas")).Returns(new Pozo());
           
            var controller = new PozoController(mock.Object);

            var view = controller.AsignarPozoPersona();

            Assert.IsInstanceOf(typeof(ViewResult), view);

        }
    }
}