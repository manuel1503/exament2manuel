﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;



namespace Interfaces
{
    public interface IPozo
    {
        List<Pozo> ListarPozos();
        List<Pozo> ByQueryAll(string query);
        void Store(Pozo pozo);
        Pozo AsignarPersonaAPozo(Pozo pozo, string nombrePersona);

    }
}
